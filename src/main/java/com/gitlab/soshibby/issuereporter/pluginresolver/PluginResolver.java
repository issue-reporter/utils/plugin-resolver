package com.gitlab.soshibby.issuereporter.pluginresolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PluginResolver {

    private static final Logger log = LoggerFactory.getLogger(PluginResolver.class);
    private BeanFinder beanFinder;

    public PluginResolver(BeanFinder beanFinder) {
        this.beanFinder = beanFinder;
    }

    public <T> List<T> resolve(List<PluginDefinition> pluginDefinition, Class<T> clazz) {
        return pluginDefinition.stream()
                .map(definition -> resolve(definition, clazz))
                .collect(Collectors.toList());
    }

    public <T> T resolve(PluginDefinition pluginDefinition, Class<T> clazz) {
        log.debug("Resolving plugin '{}'.", pluginDefinition.getClassPath());
        T plugin;

        try {
            plugin = beanFinder.find(pluginDefinition.getClassPath(), clazz);
        } catch (Exception e) {
            log.info("Failed to find plugin class '" + pluginDefinition.getClassPath() + "'. Have you forgotten to add all plugins to the plugin folder? Or have you forgotten to add '@Component' annotation to your plugin class?", e);
            throw new RuntimeException("Failed to find plugin class '" + pluginDefinition.getClassPath() + "'. Have you forgotten to add all plugins to the plugin folder? Or have you forgotten to add '@Component' annotation to your plugin class?", e);
        }

        initPlugin(plugin, pluginDefinition);
        return plugin;
    }

    private <T> void initPlugin(T plugin, PluginDefinition pluginDefinition) {
        log.debug("Initializing plugin '{}'", plugin.getClass().getSimpleName());
        Method method;

        try {
            method = plugin.getClass().getMethod("init", String.class);
        } catch (NoSuchMethodException e) {
            // No init method found, that means that the plugins isn't interested in any configs and don't need to be initialized.
            log.info("No init method found for plugin '" + pluginDefinition.getClassPath() + '.');
            return;
        }

        try {
            // Invoke the init function of the plugin.
            method.invoke(plugin, pluginDefinition.getPluginConfigAsString());
        } catch (IllegalAccessException | InvocationTargetException e) {
            log.error("Failed to invoke init method on plugin '" + pluginDefinition.getClassPath() + "'.", e);
            throw new RuntimeException("Failed to invoke init method on plugin '" + pluginDefinition.getClassPath() + "'.", e);
        }
    }
}
