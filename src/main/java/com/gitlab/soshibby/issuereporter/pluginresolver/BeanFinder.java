package com.gitlab.soshibby.issuereporter.pluginresolver;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Map;

@Component
public class BeanFinder {

    private ApplicationContext context;

    public BeanFinder(ApplicationContext context) {
        this.context = context;
    }

    public <T> T find(String className, Class<T> clazz) {
        Map<String, T> beans = context.getBeansOfType(clazz);
        Iterator<T> iterator = beans.values().iterator();

        while (iterator.hasNext()) {
            T itrClass = iterator.next();

            if (itrClass.getClass().getName().equals(className)) {
                return itrClass;
            }
        }

        throw new RuntimeException("Failed to find bean '" + className + "' of type '" + clazz.getName() + "'.");
    }
}
