package com.gitlab.soshibby.issuereporter.pluginresolver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.HashMap;

public class PluginDefinition extends HashMap {

    public String getClassPath() {
        return (String) this.get("class");
    }

    public ObjectNode getPluginConfig() {
        return new ObjectMapper().valueToTree(this);
    }

    public String getPluginConfigAsString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
